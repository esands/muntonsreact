module.exports = (req, res, next) => {
	const bots = [
		"facebot",
		"facebookexternalhit/1.0",
		"facebookexternalhit/1.1",
		"twitterbot",
		"telegrambot",
		"linkedinbot", // linkedin
		"skypeuripreview", // microsoft teams
		"pinterest",
		"discordbot",
		"disqus",
	];
	const source = req.headers["user-agent"];
	req.isSocialBot = checkArray(source.toLowerCase(), bots);
	next();
};

function checkArray(str, arr) {
	for (var i = 0; i < arr.length; i++) {
		if (str.match(arr[i])) return true;
	}
	return false;
}
