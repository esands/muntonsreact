const express = require("express");
const path = require("path");
const fs = require("fs");
const socialbot = require("./middleware/botCheck");
const app = express();

const PORT = process.env.PORT || 3000;
const indexPath = path.resolve(__dirname, "..", "build", "index.html");

// static resources should just be served as they are
app.use(
	express.static(path.resolve(__dirname, "..", "build"), { maxAge: "30d" })
);
app.get("/sharer", socialbot, async function (req, res, next) {
	if (true) {
		console.log("Here we are");
		let html = `
        <html>
            <head>
                <title>YOUR DYNAMIC TITLE</title>
                <meta property="og:title" content="xxx">
                <meta property="og:description" content="xxx">
                <meta property="og:url" content="xxx">
                <meta property="og:site_name" content="xxx">
                <meta name="twitter:title" content="xxx"/>
                <meta name="twitter:description" content="xxx">         
                <meta name="twitter:image:alt" content="xxx">
                <meta name="twitter:site" content="xxx">

            </head>
            <body>
            </body>
        </html>
        `;

		// return the html
		res.set("Content-Type", "text/html");
		res.send(html);
	} else {
		// do the permanent redirect to the CRA site
		res.set("location", "/");
		res.status(301).send();
	}
});
// listening...
app.listen(PORT, (error) => {
	if (error) {
		return console.log("Error during app startup", error);
	}
	console.log("listening on " + PORT + "...");
});
