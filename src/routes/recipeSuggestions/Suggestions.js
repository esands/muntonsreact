import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchStylesAsync, selectStyles } from "../../redux/style";
import { selectStyle, builtRecipeSelectors } from "../../redux/builtRecipe";

import ActionText from "../../components/actionText/ActionText";
import FullPageCards from "../../components/cards/FullPageCards";

const Suggestions = () => {
	const styles = useSelector(selectStyles);
	const selectedStyle = useSelector(builtRecipeSelectors.style);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(fetchStylesAsync());
	}, [dispatch]);

	return (
		<main className="container-xxl">
			<ActionText page="suggestion" />
			<FullPageCards
				selection={selectedStyle}
				list={styles}
				onClickHandler={(type) => dispatch(selectStyle(type))}
				nextStep={"/recipe-suggestions/recipe?drink_name=Pilsner"}
			/>
		</main>
	);
};

export default Suggestions;
