import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "./app/store";
import App from "./App";

test("renders Landing page react link", () => {
	const { getByRole, getAllByRole } = render(
		<Provider store={store}>
			<App />
		</Provider>
	);
	const title = getByRole("heading", { level: 1 });
	expect(title).toBeInTheDocument();

	const h2Tags = getAllByRole("heading", { level: 2 });
	expect(h2Tags[0].closest("a")).toHaveAttribute(
		"href",
		"/recipe-designer/style"
	);

	expect(h2Tags[1].closest("a")).toHaveAttribute(
		"href",
		"/recipe-suggestions/style"
	);
});
