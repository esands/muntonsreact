export const selectSelectedFlavour = (state) => state.flavour.selectedFlavour;
export const selectSelectedColour = (state) => state.colour.selectedColour;
export const selectSelectedAroma = (state) => state.aroma.selectedAroma;
export const selectSelectedABV = (state) => state.abv.selectedABV;
export const selectSelectedStyle = (state) => state.style.selectedStyle;
export const selectRecipe = (state) => [
	state.flavour.selectedFlavour,
	state.colour.selectedColour,
	state.aroma.selectedAroma,
	state.abv.selectedABV,
	state.style.selectedStyle,
];

const selectors = {
	flavour: selectSelectedFlavour,
	colour: selectSelectedColour,
	aroma: selectSelectedAroma,
	abv: selectSelectedABV,
	style: selectSelectedStyle,
	recipe: selectRecipe,
};

export default selectors;
