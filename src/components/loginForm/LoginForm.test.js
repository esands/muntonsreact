import React from "react";
import { render, fireEvent, prettyDOM } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../app/store";
import App from "../../App";

test("Shows the login form and toggle password visibility", () => {
	const { getByTestId, container } = render(
		<Provider store={store}>
			<App />
		</Provider>
	);

	expect(getByTestId("account-button")).toBeTruthy();
	fireEvent(
		getByTestId("account-button"),
		new MouseEvent("click", {
			bubbles: true,
			cancelable: true,
		})
	);
	expect(getByTestId("login-modal")).toBeTruthy();

	const passwordInput = getByTestId("login-password").querySelector("input");
	expect(passwordInput).toHaveAttribute("type", "password");

	const passwordVisibilityButton =
		getByTestId("login-password").querySelector("button");
	fireEvent(
		passwordVisibilityButton,
		new MouseEvent("click", {
			bubbles: true,
			cancelable: true,
		})
	);
	expect(passwordInput).toHaveAttribute("type", "text");
});
